provider "aws" {
  region = "us-east-2"
}

resource "aws_s3_bucket" "puppet-debugger-docs" {
  bucket = "puppet-debugger-docs"
  acl    = "public-read"
  policy = "${file("s3_public.json")}"
}

resource "aws_cloudfront_distribution" "puppet-debugger-docs" {
  enabled         = true
  is_ipv6_enabled = true

  origin {
    domain_name = "${aws_s3_bucket.puppet-debugger-docs.bucket_domain_name}"
    origin_id   = "puppet-debugger-docs"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  default_cache_behavior {
    target_origin_id = "puppet-debugger-docs"

    allowed_methods = ["GET", "HEAD"]
    cached_methods  = ["GET", "HEAD"]

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 7200
    max_ttl                = 86400
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
}