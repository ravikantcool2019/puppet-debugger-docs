+++
title = "PuppetDB Query"
date = 2020-05-06T16:12:55-07:00
weight = 5
chapter = false
#pre = "<b>X. </b>"
+++

Recent versions of puppet come equiped with a [puppetdb_query](https://puppet.com/docs/puppetdb/5.2/api/query/tutorial.html) function built-in which allows you to query puppetdb from within puppet code.  You can utilize the `puppetdb_query` function to quickly prototype the exact query you need with immediate results and without the need for curl commands inside the debugger console.

Example:

```ruby

2:>> puppetdb_query('nodes[certname]{}')
 => [
  [0] {
    "certname" => "pe-xl-db-1.puppet.vm"
  },
  [1] {
    "certname" => "pe-xl-db-0.puppet.vm"
  },
  [2] {
    "certname" => "pe-xl-core-1.puppet.vm"
  },
  [3] {
    "certname" => "pe-xl-compiler-0.puppet.vm"
  },
  [4] {
    "certname" => "pe-xl-core-0.puppet.vm"
  }
]
```

{{% notice note %}}
In order to talk to the puppetdb you must also have access to it.  For more information on setting up access to puppetdb please see the [following doc](https://puppet.com/docs/puppetdb/5.2/pdb_client_tools.html). 
{{% /notice %}}


This will require something like:

```
[root@pe-xl-core-0 /]# cat ~/.puppetlabs/client-tools/puppetdb.conf

{
  "puppetdb": {
    "server_urls": "https://localhost:8081",
    "cacert": "/etc/puppetlabs/puppet/ssl/certs/ca.pem",
    "cert": "/etc/puppetlabs/puppet/ssl/certs/localhost.pem",
    "key": "/etc/puppetlabs/puppet/ssl/private_keys/localhost.pem"
  }
}


```


A example query of resources filtering out only file types with the word puppet_enterprise in the title.

```ruby
19:>> puppetdb_query("resources[parameters, title] { title ~ 'puppet_enterprise' and type = 'File'}")
 => [
  [0] {
    "parameters" => {
      "ensure" => "absent",
       "group" => "root",
        "mode" => "0644",
       "owner" => "root"
    },
         "title" => "/opt/puppetlabs/puppet/modules/puppet_enterprise/lib/puppet/parser/functions/cookie_secret_key.rb"
  },
  [1] {
    "parameters" => {
      "ensure" => "absent",
       "group" => "root",
        "mode" => "0644",
       "owner" => "root"
    },
         "title" => "/opt/puppetlabs/puppet/modules/puppet_enterprise/lib/puppet/parser/functions/cookie_secret_key.rb"
  },
  [2] {
    "parameters" => {
       "ensure" => "directory",
        "group" => "root",
         "mode" => "0755",
        "owner" => "root",
      "require" => "Package[pe-puppet-enterprise-release]"
    },
         "title" => "/opt/puppetlabs/server/share/puppet_enterprise"
  }
]
```