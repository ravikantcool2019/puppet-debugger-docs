+++
title = "Hiera Debugging"
date = 2019-05-21T16:12:55-07:00
weight = 5
chapter = false
#pre = "<b>X. </b>"
+++

You can use the debugger to help figure out how hiera is resolving the keys in addition having access to the data it returns. Hiera lookups are nothing more than a call to a function. In fact the name of the function is called `lookup`.

Before we get into this exercise you will need to download the following example I have setup. Please run the following commands:

```
git clone https://gitlab.com/puppet-debugger/puppet-debugger-hiera-examples.git
cd puppet-debugger-hiera-examples
mkdir -p $(puppet config print confdir)
puppet config set hiera_config ${PWD}/hiera.yaml

```

Once setup you can then use the debugger to lookup values that reference the data just cloned.

Enter the following command:

1. `lookup('complex_type1')`

```
1:>> lookup('complex_type1')
 => [
  [0] {
    "name" => "value1"
  },
  [1] {
    "name" => "value2"
  },
  [2] {
    "name" => "value3"
  }
]
2:>>
```

The value being returned is defined in hiera data in the module you downloaded.

{{% notice note %}}
The debugger uses facterdb and creates a mock environment for you with facts of a real system.  These facts are fed to hiera and will influence how lookups are performed. Refer to [facterdb_filter](/reference/commands/facterdb_filter/) for information on how to mock other systems for different results. 
{{% /notice  %}}

{{% notice note %}}
Because the debugger is a long running process, hiera will cache the values. So if you change the value when the debugger is running hiera will not use the new value right away until the cache is reset. You can use the debugger's `reset` command to reset the entire environment or restart the debugger.
{{% /notice  %}}