+++
title = "Datatype Composition"
date = 2019-05-21T16:11:55-07:00
weight = 5
chapter = false
#pre = "<b>X. </b>"
+++

You can create datatypes right inside the debugger by using the `type` keyword.

This is accomplished via the `type` keyword.   This is how you define a type alias in puppet, although you might be use to putting the defination in a file instead.  What we are going to do is mock something up real quick in the debugger.

Inside the debugger type the following: 

1. `type RedhatOSType = Enum['RedHat', 'CentOS', 'OS/2']`

The above code declares a new type called RedHatOSType that will validate if the string being checked is one of the enum types specified. Go ahead and try this out.  

The puppet language has a special operator `=~` that allows you to compare the type of variable for equality. Try each of the following commands yourself to get a feel for how this works.

Then you can validate against your new datatype.

1. `'windows' =~ RedhatOSType`
2. `'redhat' =~ RedhatOSType`
3. `'RedHat' =~ RedhatOSType`

Example:
```
12:>> type RedhatOSType = Enum['RedHat', 'CentOS', 'OS/2']
13:>> 'windows' =~ RedhatOSType
 => false
14:>> 'redhat' =~ RedhatOSType
 => false
15:>> 'RedHat' =~ RedhatOSType
 => true
```

You can also utilize the stdlib `type_of` function to report what datatype a variable is. Although this may result in ambiguous data types.

```
2:>> md5('hello').upcase.type_of
 => String

```

While these is a simple example think about something more complex. Take a look at this [example](https://gitlab.com/blockops/puppet-crossbelt/blob/master/types/authorizedkeys.pp) and
[example2](https://gitlab.com/blockops/puppet-crossbelt/blob/master/types/authorizedkey.pp)

When datatypes get complex we often only check the basics because we don't understand them fully. But having a way to easily validate your validation allows you to create a more restrictive datatypes.
