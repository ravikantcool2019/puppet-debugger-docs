+++
title = "Exercises"
date = 2019-05-21T14:22:53-07:00
weight = 5
#chapter = false
#pre = "<b>X. </b>"
+++

The debugger can help you solve a plethoria of issues with your code.  Below are some common exercises that will help you solve future use cases with your code.

* [Datatype Composition](./datatype_composition/)
* [Hiera Debugging](./hiera_debugging/)
* [PuppetDB Query](./puppetdb_query/)
