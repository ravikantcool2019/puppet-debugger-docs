+++
title = "Getting Started"
date = 2019-05-21T14:22:53-07:00
weight = 3
#chapter = false
#pre = "<b>X. </b>"
+++

The puppet debugger is a puppet application so once you [install](../getting_started/installation) the gem, just fire it up using `puppet debugger`.

* [usage details](../getting_started/usage/)
* [Commands](../reference/commands/)
* [Exercises](../exercises/)
