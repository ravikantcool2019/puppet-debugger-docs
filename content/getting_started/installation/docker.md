+++
title = "Docker"
date = 2019-05-21T14:22:53-07:00
weight = 7
#chapter = false
#pre = "<b>X. </b>"
+++

If you prefer containers you can start up a vanilla Ruby container or use an existing Puppet-agent container. For a vanilla Ruby container, do the following:

```
docker run -ti ruby:2.5 /bin/bash
root@e98c7330ba7c:/# gem install puppet-debugger
root@e98c7330ba7c:/# puppet debugger

```

If you want the pre-built Puppet Debugger container image, then just run the following:

`docker run -ti nwops/puppet-debugger`

{{% notice note %}}
This image already has a few modules installed as well. So there is nothing you need to install. Don't you just love containers?
{{% /notice  %}}

Dockerfile used to build this image:

```
FROM ruby:2.6
RUN gem install puppet-debugger -N
RUN puppet module install nwops-debug && \
    puppet module install puppet-nodejs && \
    puppet module install puppetlabs-ntp
CMD /bin/bash
```
