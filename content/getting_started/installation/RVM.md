+++
title = "RVM"
date = 2019-05-21T14:22:53-07:00
weight = 7
#chapter = false
#pre = "<b>X. </b>"
+++

RVM is not supported.

If you are using RVM, then `puppet debugger` wil not work due to a "feature" in Puppet. As a workaround, you can run `puppet-debugger` executable but this works differently than the Puppet application `puppet debugger` that is used in the examples below. You may want to try the Docker method instead if that is available to you.
