+++
title = "Puppet Server"
date = 2019-05-21T14:22:53-07:00
weight = 8
#chapter = false
#pre = "<b>X. </b>"
+++


## Puppet Server 
It can be quite useful to have the puppet debugger installed on the puppet server system.  This exposes a few cool features when run under root because the debugger will have access to all the code and be able to remotely call the   node indirector without any additional setup. 

### Installing
In order to install the debugger on the puppet server we are going to follow the same procedures as the puppet-agent installation. YMMV on the actual command but this will install the debugger and place the binaries under /opt/puppetlabs/bin.

`/opt/puppetlabs/puppet/bin/gem install puppet-debugger -N --bindir /opt/puppetlabs/bin`

{{% notice warning %}}
Do not install this using the `puppetserver gem command`.
{{% /notice %}}

{{% notice info %}}
Due to the following packaging bugs [PA-2670](https://tickets.puppetlabs.com/browse/PA-2670) and [FACT-1918](https://tickets.puppetlabs.com/browse/FACT-1918) installing on the Puppet agent requires [renaming a few files](./package_bug_fix.md).  This has been fixed in newer releases, so there is no need for this step if using a recent release.

{{% /notice %}}

