+++
title = "Puppet Agent"
date = 2019-05-21T14:22:53-07:00
weight = 7
#chapter = false
#pre = "<b>X. </b>"
+++

In order to install the debugger on the puppet agent ruby we just need to utilize the gem command from the puppet agent package.  This is located under `/opt/puppetlabs/puppet/bin/gem`. You may find it useful to install the binaries somewhere else too.  In this example I install the binaries under /opt/puppetlabs/bin.

`/opt/puppetlabs/puppet/bin/gem install puppet-debugger -N --bindir /opt/puppetlabs/bin`


{{% notice warning %}}
Due to the following packaging bugs [PA-2670](https://tickets.puppetlabs.com/browse/PA-2670) and [FACT-1918](https://tickets.puppetlabs.com/browse/FACT-1918) installing on the Puppet agent requires [renaming a few files](./package_bug_fix.md).  This has been fixed in newer releases, so there is no need for this step if using a recent release.
{{% /notice %}}

