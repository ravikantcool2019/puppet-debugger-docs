+++
title = "PDK"
date = 2019-05-21T14:22:53-07:00
weight = 7
#chapter = false
#pre = "<b>X. </b>"
+++

PDK ships with the [puppet debugger](https://github.com/puppetlabs/pdk/pull/758) by default in the form of a new `pdk console` command.
You can view the [RFC](https://github.com/puppetlabs/pdk-planning/pull/44/files?short_path=836fefa#diff-836fefac153fc9b7da6f648dffd3d930
) here for the `console` command.

If you wish to keep a more updated version of the puppet-debugger gem or pin to a specific version you can
use the following syntax in your .sync.yml file.  This is not required as the debugger ships with PDK 1.14+

```yaml
# .sync.yml
Gemfile:
  required:
    ":development":
      - gem: "puppet-debugger"
        version: "~> 0.19"

pdk update
pdk bundle install
pdk bundle exec puppet debugger
```

Example invocation

```ruby
➜  puppetlabs-stdlib ✗ pdk console
Ruby Version: 2.5.7
Puppet Version: 6.13.0
Puppet Debugger Version: 0.19.0
Created by: NWOps <corey@nwops.io>
Type "commands" for a list of debugger commands
or "help" to show the help screen.


1:>>
```
