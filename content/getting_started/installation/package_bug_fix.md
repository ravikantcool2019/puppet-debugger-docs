
## Fixing the puppet packaging bug


{{% notice warning %}}
Due to the following packaging bugs [PA-2670](https://tickets.puppetlabs.com/browse/PA-2670) and [FACT-1918](https://tickets.puppetlabs.com/browse/FACT-1918) installing on the Puppet agent requires renaming a few files.
{{% /notice %}}


You can run the following script to auto-fix for both of these issues.

Failure to do this step will show an attempt to overwrite existing executables.

`facter's executable "facter" conflicts with /opt/puppetlabs/puppet/bin/facter`

Run this script on a Puppet agent installation:

```shell
cd /opt/puppetlabs/puppet/lib/ruby/gems/*/specifications
mv hiera.gemspec hiera-$(hiera -v).gemspec
mv puppet.gemspec puppet-$(puppet --version).gemspec
if [[ $(facter kernel) == 'Darwin' ]]; then
  facter_version=$(facter --version | grep -Eo '\d\.\d{1,2}\.\d')
else
  facter_version=$(facter --version | grep -Po '\d\.\d{1,2}\.\d')
fi
sed -i s/2.1.7/2.1/ facter.gemspec
mv facter.gemspec facter-${facter_version}.gemspec
```

Once complete, run the gem command to install the debugger on your Puppet agent.

`sudo /opt/puppetlabs/puppet/bin/gem install puppet-debugger --no-ri --no-rdoc`

Or if you wish to install in user space:

`/opt/puppetlabs/puppet/bin/gem install puppet-debugger --no-ri --no-rdoc --user-install`

If you see this warning, make sure you add the specified path to your path variable.

```
WARNING:  You don't have /home/user_1/.gem/ruby/2.4.0/bin in your PATH,
	  gem executables will not run
export PATH=$PATH:/home/user_1/.gem/ruby/2.4.0/bin
```