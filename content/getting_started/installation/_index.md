+++
title = "Installation"
date = 2019-05-21T14:22:53-07:00
weight = 1
#chapter = false
#pre = "<b>X. </b>"
+++

Puppet Debugger is Ruby gem that requires Puppet and Facter to be present. You can use the PDK, Puppet-agent or custom Ruby installation.

When complete, you should be able to run `puppet debugger` and see a similar prompt.

```
$ puppet debugger
Ruby Version: 2.4.5
Puppet Version: 5.5.12
Puppet Debugger Version: 0.12.2
Created by: NWOps <corey@nwops.io>
Type "commands" for a list of debugger commands
or "help" to show the help screen.


1:>>
```

### Requirements

- Ruby 2.4+

{{% notice note %}}
While Ruby 2.1 will work it is recommended you run a newer, supported version of ruby.
{{% /notice %}}

#### Supported Operating Systems

- OS X
- Linux
- Windows

#### Supported Puppet Runtimes

- Puppet 5
- Puppet 6


### Install 

I have a provided some basic instructions for each type of installation in the left column. _Use only one of the methods provided._
