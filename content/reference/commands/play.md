---
title: "Play"
date: 2019-07-03T19:17:19-04:00
draft: false
---

## Playback support
Playing back files or urls simply loads the content into the debugger session.  This means if you want to start a debugger session from an existing file or url you can play the content back in the debugger with the play command.
You can also playback a file that contains puppet code and debugger commands.

`play https://gist.githubusercontent.com/logicminds/f9b1ac65a3a440d562b0/raw`

or

`puppet debugger -p https://gist.githubusercontent.com/logicminds/f9b1ac65a3a440d562b0/raw`

or just use STDIN  `puppet debugger <some_file>` or `echo "abs(-3)" | puppet debugger`

### Web based playback support
If using the [web based debugger](https://www.puppet-debugger.com) you can playback a shared url
which would start a debugger session and then load the content from the url or parameter.

Example:
https://demo.puppet-debugger.com/play?url=https://gist.github.com/logicminds/64f0fe9f64339f18f097a9f42acd6276

or for single commands

https://demo.puppet-debugger.com/play?content=vars

Please note the web based debugger only contains a minimal amount of puppet modules.  So its likely
that your code may not work if using third party modules.  This may change in the future though.

You may have seen me (nwops) paste code snippets in the Puppet community Slack channel that references a url.  You can do this yourself with a simple gist or url hosted on the internet via the http play resource and url parameter.   

https://demo.puppet-debugger.com/play?url=https://gist.github.com/logicminds/4f6bcfd723c92aad1f01f6a800319fa4

Or inline a small code snippet with the content parameter.

https://demo.puppet-debugger.com/play?content=md5('dafsdfdsfasdfds')

The demo site is a web application that runs a local instance of the debugger per session.  

So create a link, test it and share it.