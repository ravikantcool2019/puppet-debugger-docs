---
title: "Facts"
date: 2019-07-03T19:18:27-04:00
draft: false
---

The debugger will get facts from FacterDB to speed up the inital loading process.  These facts are then used in the scope during compilation.

To list the facts in the scope simply run the `facts` command or `$::facts`

```
6:>> facts
{
 "architecture"              => "x86_64",
 "augeas"                    => {
  "version" => "1.4.0"
 },
 "augeasversion"             => "1.4.0",
 "bios_release_date"         => "12/01/2006",
 "bios_vendor"               => "innotek GmbH",
 }
 
6:>> $facts
{
 "architecture"              => "x86_64",
 "augeas"                    => {
  "version" => "1.4.0"
 },
 "augeasversion"             => "1.4.0",
 "bios_release_date"         => "12/01/2006",
 "bios_vendor"               => "innotek GmbH",
 }

```

You can force the debugger to use real facts from the current node by disablabling FacterDB:

`puppet debugger --no-facterdb --test -e '$os[family]'`

### Reference any fact
You can reference any fact just like you would inside a manfiest (because the debugger is an interactive manifest).  This is handy
if you need to know the data structure of a fact before writing code against it.

```
1:>> $os
 => {
  "architecture" => "x86_64",
        "family" => "RedHat",
      "hardware" => "x86_64",
          "name" => "Fedora",
       "release" => {
     "full" => "23",
    "major" => "23"
  },
       "selinux" => {
       "config_mode" => "permissive",
     "config_policy" => "targeted",
      "current_mode" => "permissive",
           "enabled" => true,
          "enforced" => false,
    "policy_version" => "29"
  }
}
2:>> $os[release][full]
 => "23"
```



