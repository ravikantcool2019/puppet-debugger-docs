---
title: "Environment"
date: 2019-07-03T19:17:01-04:00
draft: false
---

The environment command just prints out the currently set puppet environment. 

```
11:>> environment
Puppet Environment: production
```