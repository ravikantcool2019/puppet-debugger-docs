---
title: "Reset"
date: 2019-07-03T19:17:08-04:00
draft: false
---

The reset command throws way the current scope and allows you to start all over again without exiting the session.  This is useful 
when redefining variables or other items. 

If you just want to clear the screen you can use the `crtl-l` key combo.  (Works in any command line app)

