---
title: "Datatypes"
date: 2019-07-03T19:17:26-04:00
draft: false
---

The datatypes command will return all the datatypes found in the current module path.  This can be handy if you 
need to find a datatype to use or know which module it came from.


```
5:>> datatypes
[
  [ 0] "Error",
  [ 1] "Stdlib::Compat::Absolute_path",
  [ 2] "Stdlib::Compat::Array",
  [ 3] "Stdlib::Compat::Bool",
  [ 4] "Stdlib::Compat::Float",
  [ 5] "Stdlib::Compat::Hash",
  [ 6] "Stdlib::Compat::Integer",
  [ 7] "Stdlib::Compat::Ip_address",
  [ 8] "Stdlib::Compat::Ipv4",
  [ 9] "Stdlib::Compat::Ipv6",
  [10] "Stdlib::Compat::Numeric",
  [11] "Stdlib::Compat::String",
  [12] "Stdlib::Ensure::Service",
  [13] "Stdlib::Filemode",
  [14] "Stdlib::Filesource",
  [15] "Stdlib::Fqdn",
  [16] "Stdlib::HTTPSUrl"
  ```