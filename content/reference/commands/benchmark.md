---
title: "Benchmark"
date: 2019-07-03T19:19:06-04:00
draft: false
---

Did you know you can benchmark your puppet code? The benchmark command will time how long it takes to compile your code.

Discover which functions are slowing you down by timing them with benchmark.

{{% notice note %}}
Puppet will cache the loading of your function after initial use. The first execution will take the longest
while subsequent executions take just a fraction of the time.
{{% /notice %}}

The `benchmark` command is a toggle switch which enables/disables benchmarking. A shortcut is also provided by typing the `bm` command.

```
1>> benchmark
Benchmark Mode On
2:BM>> sha256('fdsafdfasdfsdfasdfsdfafdsfsdfasdfadsfadsfasdfasdfasdfasdf')
 => [
  [0] "d2fc2cee2205703df0245bcadceb5677703bd7ba8742a82b77683c7d45d11ff0",
  [1] "Time elapsed 6.99 ms"
]
3:BM>> sha256('fdsafdfasdfsdfasdfsdfafdsfsdfasdfadsfadsfasdfasdfasdfasdf')
 => [
  [0] "d2fc2cee2205703df0245bcadceb5677703bd7ba8742a82b77683c7d45d11ff0",
  [1] "Time elapsed 0.47 ms"
]
4:BM>> benchmark
Benchmark Mode Off

```
