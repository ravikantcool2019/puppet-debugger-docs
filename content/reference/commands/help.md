---
title: "Help"
date: 2019-07-03T19:18:04-04:00
draft: false
---

The help command displays the current runtime information for this debugger session.  Because you might have different rubies and puppet runtimes, it is helpful to have this info.


```
1>> help
Ruby Version: 2.5.1
Puppet Version: 6.4.0
Puppet Debugger Version: 0.12.3
Created by: NWOps <corey@nwops.io>
Type "commands" for a list of debugger commands
or "help" to show the help screen.

```

See the `commands` command for help with all the commands. 