+++
title = "Commands"
date = 2019-05-21T16:12:08-07:00
weight = 2
#chapter = true
#pre = "<b>X. </b>"
+++

The debugger comes with many commands that allow you to inspect the catalog, your code or reference items currently in scope.
All commands are plugins that can process, modify or perform some action on the input and/or output of the debugger.

The menu on the left details what each command does. 

