---
title: "Classes"
date: 2019-07-03T19:18:33-04:00
draft: false
---

The classes command list all the class level resources found in the current catalog.
A new catalog is compiled with each entry so the list will grow over as you use more classes.

```
4:>> include stdlib
 => Puppet::Type::Component {
  loglevel => notice,
      name => "Stdlib",
     title => "Class[Stdlib]"
}
5:>> classes
[
  [0] "settings",
  [1] "stdlib",
  [2] "stdlib::stages"
]
5:>>

```
