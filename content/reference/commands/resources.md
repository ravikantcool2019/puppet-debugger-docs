+++
title = "Resources"
date = 2019-05-21T16:12:55-07:00
weight = 5
chapter = false
#pre = "<b>X. </b>"
+++

### Summary

List all the resources currently in the catalog. Remember the puppet debugger is like an interactive manifest file. When you type puppet code, puppet will evaulate it and add it to the catalog.

```shell
1:>> include nodejs
2:>> resources
Resources not shown in any specific order
[
  [ 0] "Stage['main']",
  [ 1] "Class['Settings']",
  [ 2] "Class['main']",
  [ 3] "Node['__node_regexp__foo']",
  [ 4] "Notify['found foo node']",
  [ 5] "Class['Nodejs::Params']",
  [ 6] "Class['Nodejs']",
  [ 7] "Class['Nodejs::Install']",
  [ 8] "Package['nodejs']",
  [ 9] "Package['nodejs-devel']",
  [10] "Package['nodejs-debuginfo']",
  [11] "Package['npm']",
  [12] "File['root_npmrc']",
  [13] "Class['Nodejs::Repo::Nodesource']",
  [14] "Class['Nodejs::Repo::Nodesource::Yum']",
  [15] "Yumrepo['nodesource']",
  [16] "Yumrepo['nodesource-source']",
  [17] "File['/etc/pki/rpm-gpg/NODESOURCE-GPG-SIGNING-KEY-EL']"
]
```

A resource is the result of the evaluated type that was compiled into the catalog.  You can get a list of all the resources including the parameters that were in that resource. 

To list a resource, you first must include a resource from an installed module.

```
  5:>> include nodejs
  => Puppet::Type::Component {
  loglevel => notice,
      name => "Nodejs",
     title => "Class[Nodejs]"
}

```

OR

```
  5:>> class{'nodejs': }
  => Puppet::Type::Component {
  loglevel => notice,
      name => "Nodejs",
     title => "Class[Nodejs]"
}

```

List all the resources in the current catalog.  This shows you all the resources that were included as a result of 

```
6:>> resources
Resources not shown in any specific order
[
  [ 7] "Class['Nodejs::Params']",
  [ 8] "Class['Nodejs']",
  [ 9] "Class['Nodejs::Install']",
  [10] "Package['nodejs']",
  [11] "Package['nodejs-devel']",
  [12] "Package['nodejs-debuginfo']",
  [13] "Package['npm']",
  [14] "File['root_npmrc']",
  [15] "Class['Nodejs::Repo::Nodesource']",
  [16] "Class['Nodejs::Repo::Nodesource::Yum']",
  [17] "Yumrepo['nodesource']",
  [18] "Yumrepo['nodesource-source']",
  [19] "File['/etc/pki/rpm-gpg/NODESOURCE-GPG-SIGNING-KEY-EL']"
]
```
For additional debugging you can get a list of all the parameters of each resource.  This means listing the variables in the resource as they were evaluated.  Any hiera lookups performed or variable interpolation will be represented in the output.
The argument to `ls` is just a filter.  So if you supply a type or a resource title you will get different results.

```
6:>> ls nodejs
{
 "Class[Nodejs::Install]"               => {},
 "Class[Nodejs::Params]"                => {},
 "Class[Nodejs::Repo::Nodesource::Yum]" => {},
 "Class[Nodejs::Repo::Nodesource]"      => {},
 "Class[Nodejs]"                        => {
  "cmd_exe_path"                => nil,
  "manage_package_repo"         => true,
  "nodejs_debug_package_ensure" => "absent",
  "nodejs_debug_package_name"   => "nodejs-debuginfo",
  "nodejs_dev_package_ensure"   => "absent",
  "nodejs_dev_package_name"     => "nodejs-devel",
  "nodejs_package_ensure"       => "present",
  "nodejs_package_name"         => "nodejs",
  "npm_package_ensure"          => "absent",
  "npm_package_name"            => "npm",
  "npm_path"                    => "/usr/bin/npm",
  "npmrc_auth"                  => nil,
  "npmrc_config"                => nil,
  "package_provider"            => nil,
  "repo_class"                  => "::nodejs::repo::nodesource",
  "repo_enable_src"             => false,
  "repo_ensure"                 => "present",
  "repo_pin"                    => nil,
  "repo_priority"               => "absent",
  "repo_proxy"                  => "absent",
  "repo_proxy_password"         => "absent",
  "repo_proxy_username"         => "absent",
  "repo_release"                => nil,
  "repo_url_suffix"             => "8.x",
  "use_flags"                   => [
   [0] "npm",
   [1] "snapshot"
  ]
 },
 "Package[nodejs-debuginfo]"            => {
  "ensure"   => "absent",
  "provider" => nil,
  "tag"      => "nodesource_repo"
 },
 "Package[nodejs-devel]"                => {
  "ensure"   => "absent",
  "provider" => nil,
  "tag"      => "nodesource_repo"
 },
 "Package[nodejs]"                      => {
  "ensure"   => "present",
  "provider" => nil,
  "tag"      => "nodesource_repo"
 }
}
```
