---
title: "Types"
date: 2019-07-03T19:17:53-04:00
draft: false
---

The types command is used to list all the discoverable native resource types you can use in your puppet code.

```
11:>> types
[
  [ 0] "stage",
  [ 1] "whit",
  [ 2] "schedule",
  [ 3] "tidy",
  [ 4] "resources",
  [ 5] "group",
  [ 6] "file",
  [ 7] "notify",
  [ 8] "filebucket",
  [ 9] "exec",
  [10] "package",
  [11] "service",
  [12] "user",
  [13] "file_line",
  [14] "anchor",
  [15] "zpool",
  [16] "zfs",
  [17] "selboolean",
  [18] "selmodule",
  [19] "scheduled_task",
  [20] "sshkey",
  [21] "ssh_authorized_key",
  [22] "yumrepo",
  [23] "mount",
  [24] "augeas",
  [25] "zone",
  [26] "host",
  [27] "cron"
]
```

{{% notice note %}}
Due to technical limitations there may be items not in the list that are present in your modulepath.
{{% /notice %}}
