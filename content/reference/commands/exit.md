---
title: "Exit"
date: 2019-07-03T19:18:00-04:00
draft: false
---

The exit command will exit the current debugger session.

If you are using the `debug::break()` function and are multiple debugger sessions deep due to enumeration or looping constructs
the `exit` command will exit the current loop and allow compilation to proceed.   Additionally, `exit` commands may be required to get to the top level of the session. 