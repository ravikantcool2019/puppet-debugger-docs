FROM ruby:2.4
RUN gem install puppet-debugger --no-rdoc --no-ri && \
    gem update bundler && gem update --system
RUN puppet module install nwops-debug && \
    puppet module install puppet-nodejs && \
    puppet module install puppetlabs-ntp
CMD /bin/bash
